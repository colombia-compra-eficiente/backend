package com.example.accessingdatajpa.Entities;

import com.example.accessingdatajpa.Dto.ClientDto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Client {
    @GeneratedValue(generator="clientSequence")
    @SequenceGenerator(name="clientSequence", sequenceName="SEC_CLIENT", allocationSize=1)
    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    private Integer id;
    @Basic
    @Column(name = "NAME", nullable = false, length = 50)
    private String name;
    @Basic
    @Column(name = "EMAIL", nullable = false, length = 50)
    private String email;
    @Basic
    @Column(name = "PASSWORD", nullable = false, length = 50)
    private String password;
    @Basic
    @Column(name = "ENABLED", nullable = false, precision = 0)
    private boolean enabled;
    @Basic
    @Column(name = "CREATION_DATE", nullable = false)
    private Timestamp creationDate;
    @Basic
    @Column(name = "UPDATE_DATE", nullable = true)
    private Timestamp updateDate;
    @OneToMany(mappedBy = "client")
    private Collection<ShoppingCart> shoppingCartsById;
    @OneToMany(mappedBy = "product")
    private Collection<ShoppingCart> shoppingCartsById_0;

    public Client() { }

    public Client(ClientDto clientDto) {
        this.id = clientDto.getId();
        this.name = clientDto.getName();
        this.email = clientDto.getEmail();
        this.password = clientDto.getPassword();
    }

    public Client(ClientDto clientDto, Integer id, boolean enabled,  Timestamp creationDate) {
        this.id = id;
        this.name = clientDto.getName();
        this.email = clientDto.getEmail();
        this.password = clientDto.getPassword();
        this.enabled = enabled;
        this.creationDate = creationDate;
        this.updateDate = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return enabled == client.enabled && Objects.equals(id, client.id) && Objects.equals(name, client.name) && Objects.equals(email, client.email) && Objects.equals(password, client.password) && Objects.equals(creationDate, client.creationDate) && Objects.equals(updateDate, client.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, password, enabled, creationDate, updateDate);
    }

    public Collection<ShoppingCart> getShoppingCartsById() {
        return shoppingCartsById;
    }

    public void setShoppingCartsById(Collection<ShoppingCart> shoppingCartsById) {
        this.shoppingCartsById = shoppingCartsById;
    }

    public Collection<ShoppingCart> getShoppingCartsById_0() {
        return shoppingCartsById_0;
    }

    public void setShoppingCartsById_0(Collection<ShoppingCart> shoppingCartsById_0) {
        this.shoppingCartsById_0 = shoppingCartsById_0;
    }

    public ClientDto toDto() {
        ClientDto clientDto = new ClientDto();
        clientDto.setId(this.id);
        clientDto.setName(this.name);
        clientDto.setEmail(this.email);
        clientDto.setPassword(this.password);
        return clientDto;
    }

    @PrePersist
    public void prePersist() {
        if(this.creationDate == null)
            this.creationDate = new Timestamp(System.currentTimeMillis());
        if(!this.enabled)
            this.enabled = true;
    }

}
