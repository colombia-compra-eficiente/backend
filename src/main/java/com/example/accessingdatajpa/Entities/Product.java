package com.example.accessingdatajpa.Entities;

import com.example.accessingdatajpa.Dto.ProductDto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Product {
    @GeneratedValue(generator="productSequence")
    @SequenceGenerator(name="productSequence", sequenceName="SEC_PRODUCT", allocationSize=1)
    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    private Integer id;
    @Basic
    @Column(name = "NAME", nullable = false, length = 50)
    private String name;
    @Basic
    @Column(name = "STOCK", nullable = false, precision = 0)
    private Integer stock;
    @Basic
    @Column(name = "PRICE", nullable = true, precision = 4, scale = 2)
    private Double price;
    @Basic
    @Column(name = "ENABLED", nullable = false, precision = 0)
    private boolean enabled;
    @Basic
    @Column(name = "CREATION_DATE", nullable = false)
    private Timestamp creationDate;
    @Basic
    @Column(name = "UPDATE_DATE", nullable = true)
    private Timestamp updateDate;

    public Product() { }

    public Product(ProductDto productDto) {
        this.id = productDto.getId();
        this.name = productDto.getName();
        this.stock = productDto.getStock();
        this.price = productDto.getPrice();
    }

    public Product(ProductDto productDto, Integer id, boolean enabled, Timestamp creationDate) {
        this.id = id;
        this.name = productDto.getName();
        this.stock = productDto.getStock();
        this.price = productDto.getPrice();
        this.enabled = enabled;
        this.creationDate = creationDate;
        this.updateDate = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return enabled == product.enabled && Objects.equals(id, product.id) && Objects.equals(name, product.name) && Objects.equals(stock, product.stock) && Objects.equals(price, product.price) && Objects.equals(creationDate, product.creationDate) && Objects.equals(updateDate, product.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, stock, price, enabled, creationDate, updateDate);
    }

    public ProductDto toDto() {
        ProductDto productDto = new ProductDto();
        productDto.setId(this.id);
        productDto.setName(this.name);
        productDto.setStock(this.stock);
        productDto.setPrice(this.price);
        return productDto;
    }

    @PrePersist
    public void prePersist() {
        if(this.creationDate == null)
            this.creationDate = new Timestamp(System.currentTimeMillis());
        if(!this.enabled)
            this.enabled = true;
    }
}
