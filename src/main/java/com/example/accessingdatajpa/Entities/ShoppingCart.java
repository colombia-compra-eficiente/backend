package com.example.accessingdatajpa.Entities;

import com.example.accessingdatajpa.Dto.ProductDto;
import com.example.accessingdatajpa.Dto.ShoppingCartDto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "SHOPPING_CART", schema = "PRUEBA_TECNICA")
public class ShoppingCart {
    @GeneratedValue(generator="shoppingCartSequence")
    @SequenceGenerator(name="shoppingCartSequence", sequenceName="SEC_SHOPPING_CART", allocationSize=1)
    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    private Integer id;
    @Basic
    @Column(name = "AMOUNT", nullable = true, precision = 0)
    private Integer amount;
    @Basic
    @Column(name = "PAID", nullable = false, precision = 0)
    private boolean paid;
    @Basic
    @Column(name = "ENABLED", nullable = false, precision = 0)
    private boolean enabled;
    @Basic
    @Column(name = "CREATION_DATE", nullable = false)
    private Timestamp creationDate;
    @Basic
    @Column(name = "UPDATE_DATE", nullable = true)
    private Timestamp updateDate;
    @ManyToOne
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID", nullable = false)
    private Client client;
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID", nullable = false)
    private Product product;

    public ShoppingCart() { }

    public ShoppingCart(ShoppingCartDto shoppingCartDto) {
        this.id = shoppingCartDto.getId();
        this.amount = shoppingCartDto.getAmount();
        this.paid = shoppingCartDto.isPaid();
        this.client = new Client(shoppingCartDto.getClient());
        this.product = new Product(shoppingCartDto.getProduct());
    }

    public ShoppingCart(ShoppingCartDto shoppingCartDto, Integer id, boolean enabled, Timestamp creationDate) {
        this.id = id;
        this.amount = shoppingCartDto.getAmount();
        this.paid = shoppingCartDto.isPaid();
        this.client = new Client(shoppingCartDto.getClient());
        this.product = new Product(shoppingCartDto.getProduct());
        this.enabled = enabled;
        this.creationDate = creationDate;
        this.updateDate = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart that = (ShoppingCart) o;
        return paid == that.paid &&
                enabled == that.enabled &&
                Objects.equals(id, that.id) &&
                Objects.equals(client, that.client) &&
                Objects.equals(product, that.product) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(updateDate, that.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, product, amount, paid, enabled, creationDate, updateDate);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ShoppingCartDto toDto() {
        ShoppingCartDto productDto = new ShoppingCartDto();
        productDto.setId(this.id);
        productDto.setAmount(this.amount);
        productDto.setPaid(this.paid);
        productDto.setCreationDate(this.creationDate);
        productDto.setClient(this.client.toDto());
        productDto.setProduct(this.product.toDto());
        return productDto;
    }

    @PrePersist
    public void prePersist() {
        if(this.creationDate == null)
            this.creationDate = new Timestamp(System.currentTimeMillis());
        if(!this.enabled)
            this.enabled = true;
    }
}
