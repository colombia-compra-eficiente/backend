package com.example.accessingdatajpa.Exceptions;

public class ClientNotFoundException extends RuntimeException {
    public ClientNotFoundException(Integer id) {
        super("Client not found for this id :: " + id);
    }
}
