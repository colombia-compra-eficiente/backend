package com.example.accessingdatajpa.Exceptions;

public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(Integer id) {
        super("Product not found for this id :: " + id);
    }
}
