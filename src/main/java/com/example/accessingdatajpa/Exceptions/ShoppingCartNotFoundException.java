package com.example.accessingdatajpa.Exceptions;

public class ShoppingCartNotFoundException extends RuntimeException {
    public ShoppingCartNotFoundException(Integer id) {
        super("Shopping Cart not found for this id :: " + id);
    }
}
