package com.example.accessingdatajpa.Dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

public class ProductDto {
    private Integer id;
    private String name;
    private Integer stock;
    private Double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
