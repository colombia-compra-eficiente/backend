package com.example.accessingdatajpa.Controllers;

import com.example.accessingdatajpa.Dto.ClientDto;
import com.example.accessingdatajpa.Entities.Client;
import com.example.accessingdatajpa.Repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.accessingdatajpa.Exceptions.ClientNotFoundException;

@RestController
@RequestMapping(path="/client")
public class ClientController {
    @Autowired
    private ClientRepository clientRepository;

    @PostMapping
    public @ResponseBody ClientDto create(@RequestBody ClientDto clientDto) {
        Client client = new Client(clientDto);
        clientRepository.save(client);
        return client.toDto();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody ClientDto read(@PathVariable Integer id) {
        Client client = clientRepository.findByIdAndEnabled(id).orElseThrow(() -> new ClientNotFoundException(id));
        return client.toDto();
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<ClientDto> getAll() {
        List<ClientDto> responseList = new ArrayList<>();
        clientRepository.findByEnabledOrderById().forEach(
                object -> {
                    responseList.add(object.toDto());
                });
        return responseList;
    }

    @PutMapping(path="/{id}")
    public @ResponseBody ClientDto update(@RequestBody ClientDto clientDto, @PathVariable Integer id) {
        Client clientOld = clientRepository.findByIdAndEnabled(id).orElseThrow(() -> new ClientNotFoundException(id));
        Client clientNew = new Client(clientDto, clientOld.getId(), clientOld.isEnabled(), clientOld.getCreationDate());
        clientRepository.save(clientNew);
        return clientNew.toDto();
    }

    @DeleteMapping(path="/{id}")
    public Map<String, Boolean> delete(@PathVariable Integer id) {
        Map<String, Boolean> response = new HashMap<>();
        Client client = clientRepository.findByIdAndEnabled(id).orElseThrow(() -> new ClientNotFoundException(id));
        client.setEnabled(false);
        clientRepository.save(client);
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
