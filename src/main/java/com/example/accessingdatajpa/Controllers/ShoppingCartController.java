package com.example.accessingdatajpa.Controllers;

import com.example.accessingdatajpa.Dto.ShoppingCartDto;
import com.example.accessingdatajpa.Entities.ShoppingCart;
import com.example.accessingdatajpa.Exceptions.ShoppingCartNotFoundException;
import com.example.accessingdatajpa.Repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/shopping-cart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @PostMapping
    public @ResponseBody ShoppingCartDto create(@RequestBody ShoppingCartDto shoppingCartDto) {
        ShoppingCart shoppingCart = new ShoppingCart(shoppingCartDto);
        shoppingCartRepository.save(shoppingCart);
        return shoppingCart.toDto();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody ShoppingCartDto read(@PathVariable Integer id) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByIdAndEnabled(id).orElseThrow(() -> new ShoppingCartNotFoundException(id));
        return shoppingCart.toDto();
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<ShoppingCartDto> getAll() {
        List<ShoppingCartDto> responseList = new ArrayList<>();
        shoppingCartRepository.findByEnabledOrderByCreationDate().forEach(
                object -> {
                    responseList.add(object.toDto());
                });
        return responseList;
    }

    @PutMapping(path="/{id}")
    public @ResponseBody ShoppingCartDto update(@RequestBody ShoppingCartDto shoppingCartDto, @PathVariable Integer id) {
        ShoppingCart shoppingCartOld = shoppingCartRepository.findByIdAndEnabled(id).orElseThrow(() -> new ShoppingCartNotFoundException(id));
        ShoppingCart shoppingCartNew = new ShoppingCart(shoppingCartDto, shoppingCartOld.getId(), shoppingCartOld.isEnabled(), shoppingCartOld.getCreationDate());
        shoppingCartRepository.save(shoppingCartNew);
        return shoppingCartNew.toDto();
    }

    @DeleteMapping(path="/{id}")
    public Map<String, Boolean> delete(@PathVariable Integer id) {
        Map<String, Boolean> response = new HashMap<>();
        ShoppingCart shoppingCart = shoppingCartRepository.findByIdAndEnabled(id).orElseThrow(() -> new ShoppingCartNotFoundException(id));
        shoppingCart.setEnabled(false);
        shoppingCartRepository.save(shoppingCart);
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
