package com.example.accessingdatajpa.Controllers;

import com.example.accessingdatajpa.Dto.ProductDto;
import com.example.accessingdatajpa.Entities.Product;
import com.example.accessingdatajpa.Exceptions.ProductNotFoundException;
import com.example.accessingdatajpa.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping
    public @ResponseBody ProductDto create(@RequestBody ProductDto productDto) {
        Product product = new Product(productDto);
        productRepository.save(product);
        return product.toDto();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody ProductDto read(@PathVariable Integer id) {
        Product product = productRepository.findByIdAndEnabled(id).orElseThrow(() -> new ProductNotFoundException(id));
        return product.toDto();
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<ProductDto> getAll() {
        List<ProductDto> responseList = new ArrayList<>();
        productRepository.findByEnabledOrderById().forEach(
                object -> {
                    responseList.add(object.toDto());
                });
        return responseList;
    }

    @PutMapping(path="/{id}")
    public @ResponseBody ProductDto update(@RequestBody ProductDto productDto, @PathVariable Integer id) {
        Product productOld = productRepository.findByIdAndEnabled(id).orElseThrow(() -> new ProductNotFoundException(id));
        Product productNew = new Product(productDto, productOld.getId(), productOld.isEnabled(), productOld.getCreationDate());
        productRepository.save(productNew);
        return productNew.toDto();
    }

    @DeleteMapping(path="/{id}")
    public Map<String, Boolean> delete(@PathVariable Integer id) {
        Map<String, Boolean> response = new HashMap<>();
        Product product = productRepository.findByIdAndEnabled(id).orElseThrow(() -> new ProductNotFoundException(id));
        product.setEnabled(false);
        productRepository.save(product);
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
