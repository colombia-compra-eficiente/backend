package com.example.accessingdatajpa.Repositories;

import com.example.accessingdatajpa.Entities.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    @Query("select p from Product p where p.enabled = true order by p.id asc")
    List<Product> findByEnabledOrderById();

    @Query("select p from Product p where p.id = ?1 and p.enabled = true order by p.id asc")
    Optional<Product> findByIdAndEnabled(Integer id);
}
