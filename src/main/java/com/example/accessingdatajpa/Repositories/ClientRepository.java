package com.example.accessingdatajpa.Repositories;

import com.example.accessingdatajpa.Entities.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Integer> {
    @Query("select c from Client c where c.enabled = true order by c.id asc")
    List<Client> findByEnabledOrderById();

    @Query("select c from Client c where c.id = ?1 and c.enabled = true order by c.id asc")
    Optional<Client> findByIdAndEnabled(Integer id);
}
