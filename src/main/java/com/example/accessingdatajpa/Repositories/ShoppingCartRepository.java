package com.example.accessingdatajpa.Repositories;

import com.example.accessingdatajpa.Entities.ShoppingCart;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Integer> {
    @Query("select sc from ShoppingCart sc where sc.enabled = true order by sc.creationDate, sc.id asc")
    List<ShoppingCart> findByEnabledOrderByCreationDate();

    @Query("select sc from ShoppingCart sc where sc.id = ?1 and sc.enabled = true order by sc.creationDate, sc.id asc")
    Optional<ShoppingCart> findByIdAndEnabled(Integer id);

    @Query("select sc from ShoppingCart sc where sc.client.id = ?1 and sc.enabled = true order by sc.creationDate, sc.id asc")
    Optional<ShoppingCart> findByClientAndEnabled(Integer id);
}
