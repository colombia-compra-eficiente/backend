# backend

Este proyecto contiene la capa de servicios para la prueba técnica correspondiente.

## Requisitos

Este proyecto fue creado con las siguientes herramientas sobre Windows 11:
- Java 11
- Springboot 2.7.0
- Herramienta cliente para la prueba de los servicios REST. Para este proyecto se usó Postman (Se encuentra un script de referencia en la carpeta postman/PruebaTecnica.postman_collection.json lista para ser importada en su espacio de trabajo en postman)


## Requisitos previos

Tener una base de datos con la estructura creada en el proyecto https://gitlab.com/colombia-compra-eficiente/database


## Getting started

Para ejecutar el proyecto, se recomienda tener la variable JAVA_HOME en el path del sistema.

Desde una consola CMD se navega hasta la raíz del proyecto donde se haya clonado. Una vez ubicado ahí, se debe ejecutar el comando:
```
mvnw spring-boot:run
```

Con la ejecución de este comando, se inicia la compilación del proyecto y se ejecuta el servidor embebido con el que cuenta el proyecto, para así quedar disponible en la url http://localhost:8080.
